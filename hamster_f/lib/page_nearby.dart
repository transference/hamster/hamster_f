import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';

class NearbyRoute extends StatefulWidget {
  NearbyRoute({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _NearbyRouteState createState() => _NearbyRouteState();
}

class _NearbyRouteState extends State<NearbyRoute>
    with WidgetsBindingObserver {
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Nearby'),
      ),
      body: new FlutterMap(
        options: new MapOptions(
          center: new LatLng(43.6532, -79.3832),
          zoom: 10.0,
        ),
        layers: [
          new TileLayerOptions(
            urlTemplate: "https://api.tiles.mapbox.com/v4/"
                "{id}/{z}/{x}/{y}@2x.png?access_token={accessToken}",
            additionalOptions: {
              'accessToken': 'pk.eyJ1IjoiY29sb3VyZGVsZXRlIiwiYSI6ImNrOXc2cXNsMTA2dTYzbG5qNWJ2cnZkMWsifQ.zGD3H_mMHcxLsk9eoOKPag',
              'id': 'mapbox.streets',
            },
          ),
          new MarkerLayerOptions(
            markers: [
              new Marker(
                width: 80.0,
                height: 80.0,
                point: new LatLng(43.6532, -79.3832),
                builder: (ctx) =>
                new Container(
                  child: new Text(''),
                ),
              ),
            ],
          ),
        ],
      ),
//        body: Column(
//          children: <Widget>[
//            SingleChildScrollView(
//                child: Padding(
//                    padding: const EdgeInsets.all(8.0),
//                    child: Column(
//                        mainAxisAlignment: MainAxisAlignment.center,
//                        mainAxisSize: MainAxisSize.min,
//                        crossAxisAlignment: CrossAxisAlignment.start,
//                        children: <Widget>[
//                          Text(
//                              '⚠️ This app is work in progress. Some or all features may not be available or wrongly documented.',
//                              style: Theme.of(context).textTheme.headline),
//                          Divider(color: Colors.black),
//                          Center(
//                              child: Text('Nearlog\n',
//                                  style: Theme.of(context).textTheme.headline)),
//                          Center(child: Text('Version 0.3.2 Alpha')),
//                        ]
//                    )
//                )
//            )
//          ],
//        )
    );
  }
}
