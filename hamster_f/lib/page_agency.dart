import 'dart:async';
import 'dart:convert';
import 'page_routes.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

Future<List<Agency>> fetchPhotos(http.Client client) async {
  final response =
  await client.get('http://transferencehamster.ddns.net/api/list?t=a');
  return compute(parsePhotos, '${response.body}');
}

List<Agency> parsePhotos(String responseBody) {
  final parsed = jsonDecode(responseBody);
  final data = new List<String>.from(parsed['result']);
  return data.map<Agency>((data) => Agency.fromJson(data)).toList();
}

class Agency {
  final String id;
  final String name;
  final String region;

  Agency({this.id, this.name, this.region});

  factory Agency.fromJson(json) {
    return Agency(
      id: json as String,
      name: (json as String).toUpperCase(),
      region: 'Region',
    );
  }
}

class AgencyRoute extends StatefulWidget {
//  AgencyRoute({Key key, this.title}) : super(key: key);
//  final String title = 'TITLE';
//  var pageList;
//  var pageController;

//  AgencyRoute({Key key, this.pageList, this.pageController}) : super(key: key);

  @override
//  _AgencyRouteState createState() => _AgencyRouteState(pageList: pageList, pageController: pageController);
  _AgencyRouteState createState() => _AgencyRouteState();
}

class _AgencyRouteState extends State<AgencyRoute>
    with WidgetsBindingObserver {
//  var pageList;
//  var pageController;
//  _AgencyRouteState({Key key, this.pageList, this.pageController}) : super();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Select'),
      ),
      body: FutureBuilder<List<Agency>>(
        future: fetchPhotos(http.Client()),
        builder: (context, snapshot) {
          if (snapshot.hasError) print(snapshot.error);

          return snapshot.hasData
//            ? AgenciesList(agencies: snapshot.data, _pageList: pageList, _pageController: pageController,)
            ? AgenciesList(agencies: snapshot.data)
            : Center(child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                CircularProgressIndicator(),
                Text('\nContacting Server...'),
              ],
            )
          );
        },
      ),
    );
  }
}

class AgenciesList extends StatelessWidget {
  final List<Agency> agencies;
//  var _pageList;
//  var _pageController;

//  AgenciesList({Key key, this.agencies, this._pageList, this._pageController}) : super(key: key);
  AgenciesList({Key key, this.agencies}) : super(key: key);

  goToRoutes(String id, String name, String region, BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => RoutesRoute(
          id: id,
          name: name,
          region: region,
        ),
      ),
    );
  }

//  addRoutes(String id) {
//    _pageList.add(
//      MaterialPageRoute(
//        builder: (context) => RoutesRoute(
//          id: id,
//        ),
//      ),
//    );
//    _pageController.animateToPage(_pageList.length);
//  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: agencies.length,
      itemBuilder: (context, index) {
        return ListTile(
          title: Text('${agencies[index].name}'),
          onTap: () => goToRoutes(agencies[index].id, agencies[index].name, agencies[index].region, context),
//          onTap: () => addRoutes(agencies[index].id),
          subtitle: new Column(
            children: <Widget>[
              Row(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Flexible(
                    child: Text(
                      'Region: ${agencies[index].region}',
                      style: TextStyle(fontSize: 13.0)),
                    flex: 1,
                    fit: FlexFit.tight
                  ),
                  Flexible(
                    child: Text(
                      'ID: ${agencies[index].id}',
                      style: TextStyle(fontSize: 13.0)),
                    flex: 1,
                    fit: FlexFit.tight
                  ),
                ],
              ),
            ]
          )
        );
//        return Text('${agencies[index].id}');
      },
    );
  }
}
