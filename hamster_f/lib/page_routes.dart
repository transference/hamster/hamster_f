import 'dart:async';
import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'page_buses_2.dart';
import 'package:http/http.dart' as http;

Future<List<Route>> fetchRoutes(http.Client client, String id) async {
  final response =
  await client.get('http://transferencehamster.ddns.net/api/list?t=r&a=$id');
  return compute(parseRoute, '${response.body}');
}

List<Route> parseRoute(String responseBody) {
  final parsed = jsonDecode(responseBody);
  final data = new List<String>.from(parsed['result']);
  return data.map<Route>((data) => Route.fromJson(data)).toList();
}

class Route {
  final String id;
  final String name;
  final String region;

  Route({this.id, this.name, this.region});

  factory Route.fromJson(json) {
    return Route(
      id: json as String,
      name: (json as String).toUpperCase(),
      region: 'Region',
    );
  }
}

class RoutesRoute extends StatefulWidget {
  final String id;
  final String name;
  final String region;
  RoutesRoute({Key key, @required this.id, @required this.name, @required this.region}) : super(key: key);

  @override
  _RoutesRouteState createState() => _RoutesRouteState(id: id, name: name, region: region);
}

class _RoutesRouteState extends State<RoutesRoute>
    with WidgetsBindingObserver {

  final String id;
  final String name;
  final String region;
  _RoutesRouteState({Key key, @required this.id, @required this.name, @required this.region}) : super();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('$name'),
      ),
      body: FutureBuilder<List<Route>>(
        future: fetchRoutes(http.Client(), widget.id),
        builder: (context, snapshot) {
          if (snapshot.hasError) print(snapshot.error);
          return snapshot.hasData
            ? RoutesList(
              id: id,
              name: name,
              region: region,
              routes: snapshot.data
            )
            : Center(child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                CircularProgressIndicator(),
                Text('\nContacting Server...'),
              ],
            )
          );
        },
      ),
    );
  }
}

class RoutesList extends StatelessWidget {
  final List<Route> routes;

  final String id;
  final String name;
  final String region;
  RoutesList({Key key, @required this.id, @required this.name, @required this.region, this.routes}) : super();

  goToBuses(String idA, String nameA, String regionA, String idR, String nameR, String regionR, BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => BusesRoute(
          idR: idR,
          nameR: nameR,
          regionR: regionR,
          idA: idA,
          nameA: nameA,
          regionA: regionA,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: routes.length,
      itemBuilder: (context, index) {
        return ListTile(
          title: Text('${routes[index].name}'),
          onTap: () {
            goToBuses(id, name, region, '${routes[index].id}', '${routes[index].name}', '${routes[index].region}', context);
          },
          subtitle: new Column(
            children: <Widget>[
              Row(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Flexible(
                    child: Text(
                      'Region: ${routes[index].region}',
                      style: TextStyle(fontSize: 13.0)
                    ),
                    flex: 1,
                    fit: FlexFit.tight
                  ),
                  Flexible(
                    child: Text(
                      'ID: ${routes[index].id}',
                      style: TextStyle(fontSize: 13.0)
                    ),
                    flex: 1,
                    fit: FlexFit.tight
                  ),
                ],
              ),
            ]
          )
        );
      },
    );
  }
}
