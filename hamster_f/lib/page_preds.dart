import 'dart:async';
import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

Future<Pred> fetchPred(http.Client client, String a, String r, String b, d) async {
  final response =
  await client.get('http://transferencehamster.ddns.net/api/pred_time?a=$a&r=$r&i=$b&d=$d');
  print('RESPONSE BODY ${response.body}');
  return compute(parsePred, '${response.body}');
}

Pred parsePred(String responseBody) {
  final parsed = jsonDecode(responseBody);
  final data = parsed['result'];
  return Pred.fromJson(data);
}

class Pred {
  final String idB;
  final String nameB;
  final String regionB;
  final String idR;
  final String nameR;
  final String regionR;
  final String idA;
  final String nameA;
  final String regionA;


  Pred({this.idR, this.nameR, this.regionR, this.idB, this.nameB, this.regionB, this.idA, this.nameA, this.regionA});

  factory Pred.fromJson(json) {
    print('fromJson $json');
    return Pred(
      idR: '$json',
      nameR: '$json'.toUpperCase(),
      regionR: 'Region',
    );
  }
}

class PredsRoute extends StatefulWidget {
  final String idB;
  final String nameB;
  final String regionB;
  final String idR;
  final String nameR;
  final String regionR;
  final String idA;
  final String nameA;
  final String regionA;


  PredsRoute({this.idR, this.nameR, this.regionR, this.idB, this.nameB, this.regionB, this.idA, this.nameA, this.regionA});

  @override
  _PredsRouteState createState() => _PredsRouteState(
    idB: idB,
    nameB: nameB,
    regionB: regionB,
    idR: idR,
    nameR: nameR,
    regionR: regionR,
    idA: idA,
    nameA: nameA,
    regionA: regionA,
  );
}

class _PredsRouteState extends State<PredsRoute>
    with WidgetsBindingObserver {

  final String idB;
  final String nameB;
  final String regionB;
  final String idR;
  final String nameR;
  final String regionR;
  final String idA;
  final String nameA;
  final String regionA;

  _PredsRouteState({this.idB, this.nameB, this.regionB, this.idR, this.nameR, this.regionR, this.idA, this.nameA, this.regionA});

  var _dist = 0.0;
  final _formKey = GlobalKey<FormState>();

  bool isNumeric(String s) {
    if(s == null) {
      return false;
    }
    return double.tryParse(s) != null;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('$nameA $nameR $nameB'),
      ),
      body: Column(
        children: <Widget>[
          SizedBox(
            height: 200.0,
            child: FutureBuilder<Pred>(
              future: fetchPred(http.Client(), widget.idA, widget.idR, widget.idB, _dist),
              builder: (context, snapshot) {
                print(snapshot);
                if (snapshot.hasError) print('ERROR: ${snapshot.error}');
                return snapshot.hasData
                  ? PredBox(data: snapshot.data)
                  : Center(child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      CircularProgressIndicator(),
                      Text('\nContacting Server...'),
                      Text('${snapshot.data}'),
                    ],
                  )
                );
              },
            ),
          ),
          Builder(
            builder: (BuildContext context) {
              return Center(
                child: Column(
                  children: <Widget>[
                    Form(
                      key: _formKey,
                      child: Column(
                          children: <Widget> [
                            TextFormField(
                              validator: (value) {
                                if (value.isEmpty) {
                                  return 'Enter a number (empty)';
                                }
                                else if (isNumeric(value)) {
                                  return null;
                                }
                                else {
                                  return 'Enter a number (not a number)';
                                }
                              },
                              onChanged: (text) {
                                if (_formKey.currentState.validate()) {
                                  Scaffold.of(context).showSnackBar(SnackBar(content: Text('Processing Data')));
                                  setState(() {
                                    _dist = double.parse(text);
                                  });
                                }
                              },
                            ),
                          ]
                      ),
                    ),
                  ]
                ),
              );
            },
          ),
        ],
      ),
//      body: FutureBuilder<List<Bus>>(
//        future: fetchPreds(http.Client(), widget.idA, widget.idR, widget.idB, _dist),
//        builder: (context, snapshot) {
//          if (snapshot.hasError) print(snapshot.error);
//          return snapshot.hasData
//            ? RoutesList(agencies: snapshot.data)
//            : Center(child: Column(
//              mainAxisAlignment: MainAxisAlignment.center,
//              crossAxisAlignment: CrossAxisAlignment.center,
//              children: <Widget>[
//                CircularProgressIndicator(),
//                Text('\nContacting Server...'),
//              ],
//            )
//          );
//        },
//      ),
    );
  }
}

class PredBox extends StatelessWidget {
  final data;

  PredBox({Key key, this.data, }) : super(key: key);

//  goToPreds(String id, String name, String region, BuildContext context) {
////    Navigator.push(
////      context,
////      MaterialPageRoute(
////        builder: (context) => PredsRoute(
////          id: id,
////        ),
////      ),
////    );
//    print('NOT IMPLEMENTED YET');
//  }

  String _printDuration(Duration duration) {
    String twoDigits(int n) {
      if (n >= 10) return "$n";
      return "0$n";
    }

    String twoDigitMinutes = twoDigits(duration.inMinutes.remainder(60));
    String twoDigitSeconds = twoDigits(duration.inSeconds.remainder(60));
    return "${twoDigits(duration.inHours)}:$twoDigitMinutes:$twoDigitSeconds";
  }

  String pretty(time) {
    return time;
//    var parsed = int.tryParse(data.idR);
//    if (parsed == null) {
//      return "Error";
//    }
//    else {
//      return _printDuration(Duration(seconds: parsed));
//    }
  }

  @override
  Widget build(BuildContext context) {
//    return Text(
//      'Data: $data (end)'
//    );
    return ListView.builder(
      itemCount: 1,
      itemBuilder: (context, index) {
        return ListTile(
          title: Text('${pretty(data.idR)} seconds'),
//          onTap: goToPreds(agencies[index].idR, agencies[index].nameR, agencies[index].regionR, context),
          subtitle: new Column(
            children: <Widget>[
              Row(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Flexible(
                    child: Text(
                      '(negative seconds means you missed the bus)',
                      style: TextStyle(fontSize: 13.0)),
                    flex: 1,
                    fit: FlexFit.tight
                  ),
//                  Flexible(
//                    child: Text(
//                      'Region: ${agencies[index].regionR}',
//                      style: TextStyle(fontSize: 13.0)),
//                    flex: 1,
//                    fit: FlexFit.tight
//                  ),
//                  Flexible(
//                    child: Text(
//                      'ID: ${agencies[index].idR}',
//                      style: TextStyle(fontSize: 13.0)),
//                    flex: 1,
//                    fit: FlexFit.tight
//                  ),
                ],
              ),
            ]
          )
        );
//        return Text('${agencies[index].id}');
      },
    );
  }
}
