//import 'dart:async';
//import 'package:flutter/material.dart';
//import 'package:nearlog_app_f_2/page_intro.dart';
//import 'package:nearlog_app_f_2/page_root.dart';
//import 'package:package_info/package_info.dart';
//import 'package:shared_preferences/shared_preferences.dart';
//
//class Splash extends StatefulWidget {
//  @override
//  _SplashState createState() => new _SplashState();
//}
//
//class _SplashState extends State<Splash> with WidgetsBindingObserver {
//  pushRoot() {
//    Navigator.pushReplacement(
//        context,
//        MaterialPageRoute(
//            builder: (context) =>
//            new RootRoute(
//              title: 'Nearlog',
//            )
//        )
//    );
//  }
//
//  pushIntro(SharedPreferences prefs) async {
//    prefs.setBool('first_start', true);
//    Navigator.of(context).pushReplacement(
//        new MaterialPageRoute(
//            builder: (context) => new IntroRoute()
//        )
//    ).then(
//            (_) {
//          pushRoot();
//        }
//    );
//  }
//
//  Future checkFirstSeen() async {
//    SharedPreferences prefs = await SharedPreferences.getInstance();
//    bool _seen = (prefs.getBool('first_start') ?? false);
//    if (_seen) {
//      pushRoot();
//    } else {
//      pushIntro(prefs);
//    }
//  }
//
//  @override
//  void initState() {
//    super.initState();
//    new Timer(new Duration(milliseconds: 200), () {
//      checkFirstSeen();
//    });
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    return new Scaffold(
//      body: new Center(
//        child: Builder(
//          builder: (context) =>
//              Center(
//                child: Column(
//                  mainAxisAlignment: MainAxisAlignment.center,
//                  crossAxisAlignment: CrossAxisAlignment.center,
//                  children: <Widget>[
//                    CircularProgressIndicator(),
//                    Text('\nLoading...'),
//                  ],
//                ),
//              ),
//        ),
//      ),
//    );
//  }
//}
//
//class SharedPreferences {
//}