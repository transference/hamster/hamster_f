import 'dart:async';
import 'dart:convert';
import 'dart:math';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:hamster_f/page_preds.dart';
import 'package:http/http.dart' as http;

Future<List<Bus>> fetchBuses(http.Client client, String a, String r, d) async {
  final response =
  await client.get('http://transferencehamster.ddns.net/api/list?t=b&a=$a&r=$r');
  print('${response.body}');
  var result = parseBus(['${response.body}', client, a, r, d]);
  print('fetchBuses done?');
  return result;
//  return compute(parseBus, ['${response.body}', client, a, r, d]);
}

List<Bus> parseBus(List<dynamic> result) {
  final parsed = jsonDecode(result[0]);
  final data = new List<String>.from(parsed['result']);
  print('parseBus $result');
  print('parseBus[4] ${result[4]}');
  return data.map<Bus>((data) => Bus.fromJson(data, result[1], result[2], result[3], result[4])).toList();
}

Pred fetchPredNoAwait(http.Client client, String a, String r, String b, d) {
  print('fetchPredNoAwait');
  var result = fetchPredNoAwait_(client, a, r, b, d);
  print('fetchPredNoAwait done?');
//  var result = Pred();
  return result;
}

dynamic fetchPredNoAwait_(http.Client client, String a, String r, String b, d) async {
  print('fetchPredNoAwait_');
  return await fetchPred(client, a, r, b, d);
}

Future<Pred> fetchPred(http.Client client, String a, String r, String b, d) async {
  final response =
  await client.get('http://transferencehamster.ddns.net/api/pred_time?a=${a.toLowerCase()}&r=$r&i=$b&d=$d');
  print('http://transferencehamster.ddns.net/api/pred_time?a=${a.toLowerCase()}&r=$r&i=$b&d=$d');
  print('RESPONSE BODY ${response.body}');
  var result = compute(parsePred, '${response.body}');
//  var result = Pred();
  print('done fetchPred?');
  return result;
}

Pred parsePred(String responseBody) {
  print('parsePred');
  final parsed = jsonDecode(responseBody);
  final data = parsed['result'];
  var result = Pred.fromJson(data);
  print('done parsePred?');
  return result;
}

class Bus {
  final String idR;
  final String nameR;
  final String regionR;
  final String idA;
  final String nameA;
  final String regionA;
  final String pred;

  Bus({this.idR, this.nameR, this.regionR, this.idA, this.nameA, this.regionA, this.pred, });

  factory Bus.fromJson(json, client, a, r, d) {
    print('Bus fromJson');
    final pred = fetchPredNoAwait(client, a, r, json as String, d);
    print('Bus fromJson2 $client, $a, $r, $json, $d');
    print('$pred, ${pred.pred}');
    return Bus(
      idR: json as String,
      nameR: (json as String).toUpperCase(),
      regionR: 'Region',
      pred: pred.pred,
//      pred: '${Random().nextInt(24 00)}',
    );
  }
}

class Pred {
  final String idB;
  final String nameB;
  final String regionB;
  final String idR;
  final String nameR;
  final String regionR;
  final String idA;
  final String nameA;
  final String regionA;
  final String pred;


  Pred({this.idR, this.nameR, this.regionR, this.idB, this.nameB, this.regionB, this.idA, this.nameA, this.regionA, this.pred});

  factory Pred.fromJson(json) {
    print('Pred fromJson $json');
    return Pred(
      idR: '$json',
      nameR: '$json'.toUpperCase(),
      regionR: 'Region',
      pred: '$json',
    );
  }
}

class BusesRoute extends StatefulWidget {
  final String idR;
  final String nameR;
  final String regionR;
  final String idA;
  final String nameA;
  final String regionA;


  BusesRoute({this.idR, this.nameR, this.regionR, this.idA, this.nameA, this.regionA});

  @override
  _BusesRouteState createState() => _BusesRouteState(
    idR: idR,
    nameR: nameR,
    regionR: regionR,
    idA: idA,
    nameA: nameA,
    regionA: regionA,
  );
}

class _BusesRouteState extends State<BusesRoute>
    with WidgetsBindingObserver {

  final String idR;
  final String nameR;
  final String regionR;
  final String idA;
  final String nameA;
  final String regionA;
  final _formKey = GlobalKey<FormState>();
  var _dist = 2.7;

  _BusesRouteState({this.idR, this.nameR, this.regionR, this.idA, this.nameA, this.regionA});

  bool isNumeric(String s) {
    if(s == null) {
      return false;
    }
    return double.tryParse(s) != null;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('$nameA $nameR'),
      ),
      body:
//          Form(
//            key: _formKey,
//            child: Column(
//                children: <Widget> [
//                  TextFormField(
//                    validator: (value) {
//                      if (value.isEmpty) {
//                        return 'Enter a number (empty)';
//                      }
//                      else if (isNumeric(value)) {
//                        return null;
//                      }
//                      else {
//                        return 'Enter a number (not a number)';
//                      }
//                    },
//                    onChanged: (text) {
//                      if (_formKey.currentState.validate()) {
//                        Scaffold.of(context).showSnackBar(SnackBar(content: Text('Processing Data')));
//                        setState(() {
//                          _dist = double.parse(text);
//                        });
//                      }
//                    },
//                  ),
//                ]
//            ),
//          ),
      FutureBuilder<List<Bus>>(
        future: fetchBuses(http.Client(), widget.idA, widget.idR, _dist),
        builder: (context, snapshot) {
          if (snapshot.hasError) print(snapshot.error);
          return snapshot.hasData
            ? RoutesList(agencies: snapshot.data, idA: idA, nameA: nameA, regionA: regionA, idR: idR, nameR: nameR, regionR: regionR, )
            : Center(child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                CircularProgressIndicator(),
                Text('\nContacting Server...'),
              ],
            )
          );
        },
      )
    );
  }
}

class RoutesList extends StatelessWidget {
  final List<Bus> agencies;
  final String idA;
  final String nameA;
  final String regionA;
  final String idR;
  final String nameR;
  final String regionR;

  RoutesList({Key key, this.idA, this.nameA, this.regionA, this.idR, this.nameR, this.regionR, this.agencies}) : super(key: key);

  goToPreds(String idA, String nameA, String regionA, String idR, String nameR, String regionR,
      String idB, String nameB, String regionB, BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => PredsRoute(
          idR: idR,
          nameR: nameR,
          regionR: regionR,
          idB: idB,
          nameB: nameB,
          regionB: regionB,
          idA: idA,
          nameA: nameA,
          regionA: regionA,
        ),
      ),
    );
  }

  String _printDuration(Duration duration) {
    String twoDigits(int n) {
      if (n >= 10) return "$n";
      return "0$n";
    }

    String twoDigitMinutes = twoDigits(duration.inMinutes.remainder(60));
    String twoDigitSeconds = twoDigits(duration.inSeconds.remainder(60));
    return "${twoDigits(duration.inHours)}:$twoDigitMinutes:$twoDigitSeconds";
  }

  String pretty(time) {
    if (time == null || time == 'null') {
      return 'Error (time is null)';
    }
    var parsed = num.tryParse(time);
    if (parsed == null) {
      return 'Error (time is NaN)';
    }
    else {
      return '${(parsed/60).round()} min';
    }
//    var parsed = int.tryParse(data.idR);
//    if (parsed == null) {
//      return "Error";
//    }
//    else {
//      return _printDuration(Duration(seconds: parsed));
//    }
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: agencies.length,
      itemBuilder: (context, index) {
        print('AGENCIES INDEX ${agencies[index]}');
        print('AGENCIES INDEX PRED ${agencies[index].pred}');
        return ListTile(
          title: Text('${pretty(agencies[index].pred)}'),
          onTap: () {
            goToPreds(
              idA,
              nameA,
              regionA,
              idR,
              nameR,
              regionR,
              agencies[index].idR,
              agencies[index].nameR,
              agencies[index].regionR,
              context);
          },
          subtitle: new Column(
            children: <Widget>[
              Row(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Flexible(
                    child: Text(
                      'Region: ${agencies[index].regionR}',
                      style: TextStyle(fontSize: 13.0)),
                    flex: 1,
                    fit: FlexFit.tight
                  ),
                  Flexible(
                    child: Text(
                      'ID: ${agencies[index].idR}',
                      style: TextStyle(fontSize: 13.0)),
                    flex: 1,
                    fit: FlexFit.tight
                  ),
                ],
              ),
            ]
          )
        );
      },
    );
  }
}
