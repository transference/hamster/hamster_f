import 'package:flutter/material.dart';

class SettingsRoute extends StatefulWidget {
  SettingsRoute({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _SettingsRouteState createState() => _SettingsRouteState();
}

class _SettingsRouteState extends State<SettingsRoute>
    with WidgetsBindingObserver {
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Settings'),
        ),
        body: Column(
          children: <Widget>[
            SingleChildScrollView(
                child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                              '⚠️ This app is work in progress. Some or all features may not be available or wrongly documented.',
                              style: Theme.of(context).textTheme.headline),
                          Divider(color: Colors.black),
                          Center(
                              child: Text('Transference Hamster Flutter\n',
                                  style: Theme.of(context).textTheme.headline)),
                          Center(child: Text('Version 0.1.0 Alpha')),
                        ]
                    )
                )
            )
          ],
        )
    );
  }
}
