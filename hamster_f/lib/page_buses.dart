import 'dart:async';
import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:hamster_f/page_preds.dart';
import 'package:http/http.dart' as http;

Future<List<Bus>> fetchBuses(http.Client client, String a, String r) async {
  final response =
  await client.get('http://transferencehamster.ddns.net/api/list?t=b&a=$a&r=$r');
//  print('${response.body}');
  return compute(parseBus, '${response.body}');
}

List<Bus> parseBus(String responseBody) {
  final parsed = jsonDecode(responseBody);
  final data = new List<String>.from(parsed['result']);
  return data.map<Bus>((data) => Bus.fromJson(data)).toList();
}

class Bus {
  final String idR;
  final String nameR;
  final String regionR;
  final String idA;
  final String nameA;
  final String regionA;


  Bus({this.idR, this.nameR, this.regionR, this.idA, this.nameA, this.regionA});

  factory Bus.fromJson(json) {
    return Bus(
      idR: json as String,
      nameR: (json as String).toUpperCase(),
      regionR: 'Region',
    );
  }
}

class BusesRoute extends StatefulWidget {
  final String idR;
  final String nameR;
  final String regionR;
  final String idA;
  final String nameA;
  final String regionA;


  BusesRoute({this.idR, this.nameR, this.regionR, this.idA, this.nameA, this.regionA});

  @override
  _BusesRouteState createState() => _BusesRouteState(
    idR: idR,
    nameR: nameR,
    regionR: regionR,
    idA: idA,
    nameA: nameA,
    regionA: regionA,
  );
}

class _BusesRouteState extends State<BusesRoute>
    with WidgetsBindingObserver {

  final String idR;
  final String nameR;
  final String regionR;
  final String idA;
  final String nameA;
  final String regionA;

  _BusesRouteState({this.idR, this.nameR, this.regionR, this.idA, this.nameA, this.regionA});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('$nameA $nameR'),
      ),
      body: FutureBuilder<List<Bus>>(
        future: fetchBuses(http.Client(), widget.idA, widget.idR),
        builder: (context, snapshot) {
          if (snapshot.hasError) print(snapshot.error);
          return snapshot.hasData
            ? RoutesList(agencies: snapshot.data, idA: idA, nameA: nameA, regionA: regionA, idR: idR, nameR: nameR, regionR: regionR, )
            : Center(child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                CircularProgressIndicator(),
                Text('\nContacting Server...'),
              ],
            )
          );
        },
      ),
    );
  }
}

class RoutesList extends StatelessWidget {
  final List<Bus> agencies;
  final String idA;
  final String nameA;
  final String regionA;
  final String idR;
  final String nameR;
  final String regionR;

  RoutesList({Key key, this.idA, this.nameA, this.regionA, this.idR, this.nameR, this.regionR, this.agencies}) : super(key: key);

  goToPreds(String idA, String nameA, String regionA, String idR, String nameR, String regionR,
      String idB, String nameB, String regionB, BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => PredsRoute(
          idR: idR,
          nameR: nameR,
          regionR: regionR,
          idB: idB,
          nameB: nameB,
          regionB: regionB,
          idA: idA,
          nameA: nameA,
          regionA: regionA,
        ),
      ),
    );
    print('NOT IMPLEMENTED YET');
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: agencies.length,
      itemBuilder: (context, index) {
        return ListTile(
          title: Text('${agencies[index].idR}'),
          onTap: () {
            goToPreds(
                idA, nameA, regionA,
                idR, nameR, regionR,
                agencies[index].idR, agencies[index].nameR, agencies[index].regionR,
                context);
          },
          subtitle: new Column(
            children: <Widget>[
              Row(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Flexible(
                    child: Text(
                      'Region: ${agencies[index].regionR}',
                      style: TextStyle(fontSize: 13.0)),
                    flex: 1,
                    fit: FlexFit.tight
                  ),
                  Flexible(
                    child: Text(
                      'ID: ${agencies[index].idR}',
                      style: TextStyle(fontSize: 13.0)),
                    flex: 1,
                    fit: FlexFit.tight
                  ),
                ],
              ),
            ]
          )
        );
//        return Text('${agencies[index].id}');
      },
    );
  }
}
